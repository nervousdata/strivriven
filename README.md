# Strivriven

Contribution for [NaNoGenMo (National Novel Generation Month) 2021](https://github.com/NaNoGenMo/2021/issues)

**Diffusing a sentence. Let it expand and propagate in a somehow ‘electrical’ way.**

- ImageMagick 7 and Bash/Shell scripting.
- The program crops a portion of an image of a sentence ~60.000 times. The size of each piece is 1/12 of the sentence.
- After every cut the position where the ‘punch’ is put, moves left or right (on x-axis). The movement follows a sine wave. There is some frequency modulation going on.
- Placing the snippets: 10 per row, 30 rows per page. The order follows the time of cutting.
- Input: A sentence taken from the introduction to [Heinrich Hertz’ book on Electric Waves](https://archive.org/details/b2172457x/mode/2up) from 1893.
