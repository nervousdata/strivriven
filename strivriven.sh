#!/bin/bash

bz=-1
until [ $bz -gt  60298 ] # 201 pages
do
  ((bz+=1))
  bx=`printf %05d $bz`
  hz=`awk -v x="$bz" 'BEGIN {wv=sin(sin(4*3.14159265*(x/1000)) * 3*3.14159265*(x/2000 +1))+0.9999 ; printf wv }'`
  mkdir -p cuts book # create directory for the snippets and new pages
  echo " Cutting … $bx "
  magick hzz.png -gravity SouthWest -crop "%[fx:(w/12)]"x"%[fx:h]"+"%[fx:(w/2.2)*$hz]"+0 +repage cuts/$bx.png
done

 while :
 do
   echo ' Making new pages … '
   montage cuts/%05d.png[00000-20099] -tile 10x30 -gravity center -background white -geometry +0+0 -units PixelsPerInch -density 150 book/1_%02d.png
   montage cuts/%05d.png[20100-40199] -tile 10x30 -gravity center -background white -geometry +0+0 -units PixelsPerInch -density 150 book/2_%02d.png
   montage cuts/%05d.png[40200-60299] -tile 10x30 -gravity center -background white -geometry +0+0 -units PixelsPerInch -density 150 book/3_%02d.png
   break
 done
