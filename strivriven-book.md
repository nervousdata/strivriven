---
title: "Strivriven"
author: "Jasmin Meerhoff"
date: "November 2021"
documentclass: article
classoption: oneside
header-includes: |
  \usepackage[english]{babel}
  \usepackage[a5paper, left=28mm, right=20mm]{geometry}
  \usepackage[document]{ragged2e}
  \usepackage{caption}   
  \captionsetup[figure]{labelformat=empty}
fontsize: 12pt
fontfamily: Alegreya
output: pdf_document
---

\begin{center}Based on a sentence from Heinrich Hertz’ Electrical Waves. Being Researches On The Propagation Of Electric Action With Finite Velocity Through Space (1893)\end{center}


![](book/1_00.png){ width=110% margin=auto }

![](book/1_01.png){ width=110% margin=auto }

![](book/1_02.png){ width=110% margin=auto }

![](book/1_03.png){ width=110% margin=auto }

![](book/1_04.png){ width=110% margin=auto }

![](book/1_05.png){ width=110% margin=auto }

![](book/1_06.png){ width=110% margin=auto }

![](book/1_07.png){ width=110% margin=auto }

![](book/1_08.png){ width=110% margin=auto }

![](book/1_09.png){ width=110% margin=auto }

![](book/1_10.png){ width=110% margin=auto }

![](book/1_11.png){ width=110% margin=auto }

![](book/1_12.png){ width=110% margin=auto }

![](book/1_13.png){ width=110% margin=auto }

![](book/1_14.png){ width=110% margin=auto }

![](book/1_15.png){ width=110% margin=auto }

![](book/1_16.png){ width=110% margin=auto }

![](book/1_17.png){ width=110% margin=auto }

![](book/1_18.png){ width=110% margin=auto }

![](book/1_19.png){ width=110% margin=auto }

![](book/1_20.png){ width=110% margin=auto }

![](book/1_21.png){ width=110% margin=auto }

![](book/1_22.png){ width=110% margin=auto }

![](book/1_23.png){ width=110% margin=auto }

![](book/1_24.png){ width=110% margin=auto }

![](book/1_25.png){ width=110% margin=auto }

![](book/1_26.png){ width=110% margin=auto }

![](book/1_27.png){ width=110% margin=auto }

![](book/1_28.png){ width=110% margin=auto }

![](book/1_29.png){ width=110% margin=auto }

![](book/1_30.png){ width=110% margin=auto }

![](book/1_31.png){ width=110% margin=auto }

![](book/1_32.png){ width=110% margin=auto }

![](book/1_33.png){ width=110% margin=auto }

![](book/1_34.png){ width=110% margin=auto }

![](book/1_35.png){ width=110% margin=auto }

![](book/1_36.png){ width=110% margin=auto }

![](book/1_37.png){ width=110% margin=auto }

![](book/1_38.png){ width=110% margin=auto }

![](book/1_39.png){ width=110% margin=auto }

![](book/1_40.png){ width=110% margin=auto }

![](book/1_41.png){ width=110% margin=auto }

![](book/1_42.png){ width=110% margin=auto }

![](book/1_43.png){ width=110% margin=auto }

![](book/1_44.png){ width=110% margin=auto }

![](book/1_45.png){ width=110% margin=auto }

![](book/1_46.png){ width=110% margin=auto }

![](book/1_47.png){ width=110% margin=auto }

![](book/1_48.png){ width=110% margin=auto }

![](book/1_49.png){ width=110% margin=auto }

![](book/1_50.png){ width=110% margin=auto }

![](book/1_51.png){ width=110% margin=auto }

![](book/1_52.png){ width=110% margin=auto }

![](book/1_53.png){ width=110% margin=auto }

![](book/1_54.png){ width=110% margin=auto }

![](book/1_55.png){ width=110% margin=auto }

![](book/1_56.png){ width=110% margin=auto }

![](book/1_57.png){ width=110% margin=auto }

![](book/1_58.png){ width=110% margin=auto }

![](book/1_59.png){ width=110% margin=auto }

![](book/1_60.png){ width=110% margin=auto }

![](book/1_61.png){ width=110% margin=auto }

![](book/1_62.png){ width=110% margin=auto }

![](book/1_63.png){ width=110% margin=auto }

![](book/1_64.png){ width=110% margin=auto }

![](book/1_65.png){ width=110% margin=auto }

![](book/1_66.png){ width=110% margin=auto }

![](book/2_00.png){ width=110% margin=auto }

![](book/2_01.png){ width=110% margin=auto }

![](book/2_02.png){ width=110% margin=auto }

![](book/2_03.png){ width=110% margin=auto }

![](book/2_04.png){ width=110% margin=auto }

![](book/2_05.png){ width=110% margin=auto }

![](book/2_06.png){ width=110% margin=auto }

![](book/2_07.png){ width=110% margin=auto }

![](book/2_08.png){ width=110% margin=auto }

![](book/2_09.png){ width=110% margin=auto }

![](book/2_10.png){ width=110% margin=auto }

![](book/2_11.png){ width=110% margin=auto }

![](book/2_12.png){ width=110% margin=auto }

![](book/2_13.png){ width=110% margin=auto }

![](book/2_14.png){ width=110% margin=auto }

![](book/2_15.png){ width=110% margin=auto }

![](book/2_16.png){ width=110% margin=auto }

![](book/2_17.png){ width=110% margin=auto }

![](book/2_18.png){ width=110% margin=auto }

![](book/2_19.png){ width=110% margin=auto }

![](book/2_20.png){ width=110% margin=auto }

![](book/2_21.png){ width=110% margin=auto }

![](book/2_22.png){ width=110% margin=auto }

![](book/2_23.png){ width=110% margin=auto }

![](book/2_24.png){ width=110% margin=auto }

![](book/2_25.png){ width=110% margin=auto }

![](book/2_26.png){ width=110% margin=auto }

![](book/2_27.png){ width=110% margin=auto }

![](book/2_28.png){ width=110% margin=auto }

![](book/2_29.png){ width=110% margin=auto }

![](book/2_30.png){ width=110% margin=auto }

![](book/2_31.png){ width=110% margin=auto }

![](book/2_32.png){ width=110% margin=auto }

![](book/2_33.png){ width=110% margin=auto }

![](book/2_34.png){ width=110% margin=auto }

![](book/2_35.png){ width=110% margin=auto }

![](book/2_36.png){ width=110% margin=auto }

![](book/2_37.png){ width=110% margin=auto }

![](book/2_38.png){ width=110% margin=auto }

![](book/2_39.png){ width=110% margin=auto }

![](book/2_40.png){ width=110% margin=auto }

![](book/2_41.png){ width=110% margin=auto }

![](book/2_42.png){ width=110% margin=auto }

![](book/2_43.png){ width=110% margin=auto }

![](book/2_44.png){ width=110% margin=auto }

![](book/2_45.png){ width=110% margin=auto }

![](book/2_46.png){ width=110% margin=auto }

![](book/2_47.png){ width=110% margin=auto }

![](book/2_48.png){ width=110% margin=auto }

![](book/2_49.png){ width=110% margin=auto }

![](book/2_50.png){ width=110% margin=auto }

![](book/2_51.png){ width=110% margin=auto }

![](book/2_52.png){ width=110% margin=auto }

![](book/2_53.png){ width=110% margin=auto }

![](book/2_54.png){ width=110% margin=auto }

![](book/2_55.png){ width=110% margin=auto }

![](book/2_56.png){ width=110% margin=auto }

![](book/2_57.png){ width=110% margin=auto }

![](book/2_58.png){ width=110% margin=auto }

![](book/2_59.png){ width=110% margin=auto }

![](book/2_60.png){ width=110% margin=auto }

![](book/2_61.png){ width=110% margin=auto }

![](book/2_62.png){ width=110% margin=auto }

![](book/2_63.png){ width=110% margin=auto }

![](book/2_64.png){ width=110% margin=auto }

![](book/2_65.png){ width=110% margin=auto }

![](book/2_66.png){ width=110% margin=auto }

![](book/3_00.png){ width=110% margin=auto }

![](book/3_01.png){ width=110% margin=auto }

![](book/3_02.png){ width=110% margin=auto }

![](book/3_03.png){ width=110% margin=auto }

![](book/3_04.png){ width=110% margin=auto }

![](book/3_05.png){ width=110% margin=auto }

![](book/3_06.png){ width=110% margin=auto }

![](book/3_07.png){ width=110% margin=auto }

![](book/3_08.png){ width=110% margin=auto }

![](book/3_09.png){ width=110% margin=auto }

![](book/3_10.png){ width=110% margin=auto }

![](book/3_11.png){ width=110% margin=auto }

![](book/3_12.png){ width=110% margin=auto }

![](book/3_13.png){ width=110% margin=auto }

![](book/3_14.png){ width=110% margin=auto }

![](book/3_15.png){ width=110% margin=auto }

![](book/3_16.png){ width=110% margin=auto }

![](book/3_17.png){ width=110% margin=auto }

![](book/3_18.png){ width=110% margin=auto }

![](book/3_19.png){ width=110% margin=auto }

![](book/3_20.png){ width=110% margin=auto }

![](book/3_21.png){ width=110% margin=auto }

![](book/3_22.png){ width=110% margin=auto }

![](book/3_23.png){ width=110% margin=auto }

![](book/3_24.png){ width=110% margin=auto }

![](book/3_25.png){ width=110% margin=auto }

![](book/3_26.png){ width=110% margin=auto }

![](book/3_27.png){ width=110% margin=auto }

![](book/3_28.png){ width=110% margin=auto }

![](book/3_29.png){ width=110% margin=auto }

![](book/3_30.png){ width=110% margin=auto }

![](book/3_31.png){ width=110% margin=auto }

![](book/3_32.png){ width=110% margin=auto }

![](book/3_33.png){ width=110% margin=auto }

![](book/3_34.png){ width=110% margin=auto }

![](book/3_35.png){ width=110% margin=auto }

![](book/3_36.png){ width=110% margin=auto }

![](book/3_37.png){ width=110% margin=auto }

![](book/3_38.png){ width=110% margin=auto }

![](book/3_39.png){ width=110% margin=auto }

![](book/3_40.png){ width=110% margin=auto }

![](book/3_41.png){ width=110% margin=auto }

![](book/3_42.png){ width=110% margin=auto }

![](book/3_43.png){ width=110% margin=auto }

![](book/3_44.png){ width=110% margin=auto }

![](book/3_45.png){ width=110% margin=auto }

![](book/3_46.png){ width=110% margin=auto }

![](book/3_47.png){ width=110% margin=auto }

![](book/3_48.png){ width=110% margin=auto }

![](book/3_49.png){ width=110% margin=auto }

![](book/3_50.png){ width=110% margin=auto }

![](book/3_51.png){ width=110% margin=auto }

![](book/3_52.png){ width=110% margin=auto }

![](book/3_53.png){ width=110% margin=auto }

![](book/3_54.png){ width=110% margin=auto }

![](book/3_55.png){ width=110% margin=auto }

![](book/3_56.png){ width=110% margin=auto }

![](book/3_57.png){ width=110% margin=auto }

![](book/3_58.png){ width=110% margin=auto }

![](book/3_59.png){ width=110% margin=auto }

![](book/3_60.png){ width=110% margin=auto }

![](book/3_61.png){ width=110% margin=auto }

![](book/3_62.png){ width=110% margin=auto }

![](book/3_63.png){ width=110% margin=auto }

![](book/3_64.png){ width=110% margin=auto }

![](book/3_65.png){ width=110% margin=auto }

![](book/3_66.png){ width=110% margin=auto }
